# Ag Cap

 TBD: collect stuff about Andy Glew's various capability  security related things, e.g. 1985-1991,  2004, 2005-2009, 2022

 Darn! I missed the opportunity to call this "Andy Capp",  referring to my favorite comic strips of all time. https://en.wikipedia.org/wiki/Andy_Capp

 ## Mostly  documentation, mostly wiki

 This project will be mostly documentation, mostly  wiki.   If code exists it will probably mostly be scripts related to managing the wiki. 

 
See https://gitlab.com/AndyGlew/Ag-Cap/-/wikis/home
versus https://gitlab.com/AndyGlew/Ag-Cap/-/blob/main/README.md

 (TBD:  determine relative linking patterns  between GitLab code and wiki web views.)

## TBD: Support

TBD: issue tracker, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Project status
2022-08-03: Starting on GitLab

Note:  similar stuff may be scattered randomly around the Internet.  but this time I'm really serious ... well, probably not that serious, because the GitLab wiki doesn't really have copy/paste of images, and I'm unlikely to be very motivated until I can do drawings. But I want to start.
